/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.albumproject.service;

import com.soi.albumproject.dao.SaleDao;
import com.soi.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author user
 */
public class ReportService {

    public List<ReportSale> getreportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getreportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
}
